class Program8 {
    public static void main(String[] args) {
        
        int num = 987654321;
        int numVal = 0;
        int temp = num;

        for(int i = temp; i != 0; i /= 10) {
            numVal = numVal * 10 + (i % 10);
        }
        System.out.println(numVal);
        
    }
}
