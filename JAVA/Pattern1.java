public class Pattern1 {
    public static void main(String[] args) {
        int row = 4;
        int num =(row*(row+1))/2;
        for(int i=1;i<=row;i++){
            
            for(int j=row-1+1;j>=1;j--){
                System.out.print(num+"\t");
                num--;
            }
            System.out.println();
        }
    }
}
