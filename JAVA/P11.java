class P11 {

    public static void main(String[] args) {
        int sum = 0;
        int product =1;

        int num =256985;
        int temp=num;
        while(temp!=0){

            int rem = temp%10;
            if (rem%2==0) {
                sum+=rem;
            }else{
                product*=rem;
            }
            temp/=10;
        }
        System.out.println("Sum of even digits: "+sum);
        System.out.println("Product of odd digits: "+product);
    }
}
